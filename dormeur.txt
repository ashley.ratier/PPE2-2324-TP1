                 Le dormeur du val 

c'est un trou de verdure où chante une rivière,
accrochant follement aux herbes des haillons
d'argent ; où le soleil, de la montagne fière,
luit : c'est un petit val qui mousse de rayons. 

un soldat jeune, bouche ouverte, tête nue,
et la nuque baignant dans le frais cresson bleu,
dort ; il est étendu dans l'herbe, sous la nue,
pâle dans son lit vert où la lumière pleut. 

les pieds dans les glaïeuls, il dort. Souriant comme
sourirait un enfant malade, il fait un somme :
nature, berce-le chaudement : il a froid. 

les parfums ne font pas frissonner sa narine ;
il dort dans le soleil, la main sur sa poitrine,
tranquille. Il a deux trous rouges au côté droit. 

           Arthur Rimbaud
